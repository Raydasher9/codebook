let k, pl

class Main extends Phaser.Scene {

  preload() {
    this.load.image('background', './assets/img/background.png')
    this.load.image('coin', './assets/img/coin.png')
    this.load.image('gameover', './assets/img/gameover.png')
    this.load.image('platform', './assets/img/platform.png')
    this.load.image('player', './assets/img/player.png')
    this.load.image('villain', './assets/img/villain.png')

    this.load.audio('music', 'assets/snd/background.wav')
    this.load.audio('bump', 'assets/snd/bump.wav')
    this.load.audio('pickup', 'assets/snd/coin.wav')
    this.load.audio('gs', 'assets/snd/gs.wav')
    this.load.audio('jump', 'assets/snd/jump.mp3')
  }

  create() {



    let music = this.sound.add('music')
    let bump = this.sound.add('bump')
    let pickup = this.sound.add('pickup')
    let gs = this.sound.add('gs')
    let jump = this.sound.add('jump')

    this.add.image(0, 0, 'background').setOrigin(0, 0)

    pl = this.physics.add.sprite(500, 500, 'player')
    pl.setGravityY(900)
    pl.setCollideWorldBounds(true)

    k = this.input.keyboard.addKeys('LEFT,RIGHT,SPACE,W,A,S,D,R')

    let enemies = this.physics.add.group()

    const spawnEnemy = (x=200,y=300) => {
      let enemy = enemies.create(x=200,y=200, 'villain')
      enemy.setVelocity(300)
      enemy.setCollideWorldBounds(true)
      enemy.setBounce(1)
    }
    spawnEnemy()
    spawnEnemy(100,500)
    spawnEnemy(200,300)
    spawnEnemy(400,300)

    this.physics.add.collider(pl, enemies)
  }

  

  update() {
    if (k.LEFT.isDown || k.A.isDown) {
      pl.setVelocityX(-400)
    }
    if (k.RIGHT.isDown || k.A.isDown) {
      pl.setVelocityX(400)
    }
    if (k.SPACE.isDown || k.W.isDown) {
      pl.setVelocityY(-700)
    }
    if (pl.body.onFloor()) {
      pl.setDragX(900)
    } else {
      pl.setDragX(0)
    }
    if (k.R.isDown) {
      this.scene.restart()
    }
  }


}

const game = new Phaser.Game({
  scene: Main,
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
    }
  }
})

// justice
