// declare the variable name


let name

// assign the string 'Nick' to variable name

name = 'Nick'

// print a greeting to the console using the variable name
console.log(`Hello ${name}`)